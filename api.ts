import { array, id, object, string, values } from 'cast.ts'
import { readFileSync, writeFileSync } from 'fs'
import { chromium } from 'playwright'

export type Stock = {
  stockId: number
  code: string
  name: string
}

const searchResultParser = object({
  more: values(['1', '0']),
  stockInfo: array(
    object({
      stockId: id(),
      code: string(),
      name: string(),
    }),
  ),
})

export async function searchStockInfo(options: {
  type?: string // default is 'A'
  name: string // e.g. 'green'
  market?: string // default is 'SEHK'
}) {
  let params = new URLSearchParams({
    type: options.type || 'A',
    name: options.name,
    market: options.market || 'SEHK',
    lang: 'EN',
    callback: 'callback',
  })
  let url = `https://www1.hkexnews.hk/search/partial.do?${params}`
  let res = await fetch(url)
  let text = await res.text()
  text = text
    .trim()
    .replace(/^callback\(/, '')
    .replace(/\);$/, '')
  let json = JSON.parse(text)
  let output = searchResultParser.parse(json)
  return output.stockInfo
}

export async function searchByStockId(options: {
  stockId: number
  market?: string // default is 'SEHK'
  from?: string // default is '19990401'
  to?: string // default is today
}) {
  let params = new URLSearchParams({
    lang: 'EN',
    market: options.market || 'SEHK',
    searchType: '0',
    documentType: '',
    t1code: '',
    t2Gcode: '',
    t2code: '',
    stockId: String(options.stockId),
    category: '0',
    title: '',
  })
  let url = `https://www1.hkexnews.hk/search/titlesearch.xhtml`
  let res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: params,
  })
  let html = await res.text()
  let documents = await parseHTML(html)
  return documents
}

let browserP = chromium.launch({ headless: true })
let pageP = browserP.then(async browser => {
  let page = await browser.newPage()
  await page.goto('https://www1.hkexnews.hk')
  return page
})

export async function parseHTML(html: string) {
  let page = await pageP
  let documents = await page.evaluate(html => {
    let doc = new DOMParser().parseFromString(html, 'text/html')
    return Array.from(doc.querySelectorAll('table tbody tr'), tr => {
      let div = tr.querySelector('.release-time')
      div?.querySelectorAll('span').forEach(span => span.remove())
      let releaseTime = div?.textContent?.trim()
      let headline = tr.querySelector('.headline')?.textContent?.trim()
      let a = tr.querySelector('.doc-link a') as HTMLAnchorElement
      let href = a.href
      let name = a.textContent?.trim()
      let size = tr.querySelector('.attachment_filesize')?.textContent?.trim()
      let type = tr.querySelector('.attachment_filesize + span')?.className
      return { releaseTime, headline, href, name, size, type }
    })
  }, html)
  return documents
}

async function main() {
  // searchStockInfo({ name: 'green' })
  // let documents = await searchByStockId({ stockId: 1000137525 })
  // let html = readFileSync('sample.html').toString()
  // parseHTML(html)
  // console.log({ documents })
}
main().catch(e => console.error(e))
